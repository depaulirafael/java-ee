package br.com.fgp.arquitetura.software.java.rest;


import br.com.fgp.arquitetura.software.java.model.Evento;
import br.com.fgp.arquitetura.software.java.persistence.EventoPersist;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author Filipe Bojikian Rissi
 * @since 1.0
 */
@Path("evento")
public class EventoResource {

    @Inject
    private EventoPersist eventoPersist;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Evento> getJson() {
        return eventoPersist.listar();
    }

}
