package br.com.fgp.arquitetura.software.java.model;

import javax.json.JsonObject;
import javax.persistence.*;

/**
 * @author Filipe Bojikian Rissi
 * @since 1.0
 */

@Entity
@NamedQueries({
        @NamedQuery(name = "Evento.getAll", query = "select e from Evento e")
})
public class Evento {

    @Id
    @GeneratedValue
    private long id;
    private String data;
    private String nome;
    private String descricao;

    public Evento() {
    }

    public Evento(JsonObject jsonObject) {
        this.data = jsonObject.getString("data");
        this.nome = jsonObject.getString("nome");
        this.descricao = jsonObject.getString("descricao");
    }

    public Evento(String data, String nome, String descricao) {
        this.data = data;
        this.nome = nome;
        this.descricao = descricao;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
